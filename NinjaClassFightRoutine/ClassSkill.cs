﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugins;

namespace NinjaClassFightRoutine
{
    public class ClassSkill : IComparable<ClassSkill>, IComparer<ClassSkill>
    {
        public int Priority { get; set; }

        public uint Id { get; set; }
        public float Cooldown { get; set; }
        public DateTime LastCast { get; set; }
        public bool DebuffOnly { get; set; }
        public int MinStacks { get; set; }


        public enum Spell
        {
            DevoreurVorace = 52709,
            Embuchesinistre = 52710,
            Elemental_Ascension = 52708,
            Fendglace = 52604,
            Shadow_Assault = 52605,
            Great_Fireball = 52606,
            Shuriken_Toss = 52701,
            Maple_Star  = 52702,
            Blood_Corruption = 52703,
            Shuriken_Storm = 52704,
            Razor_Wind = 52707,
        }

        public ClassSkill(uint id, float cooldown, int priority, bool debuff = false, int minStacks = 1)
        {
            Id = id;
            Cooldown = cooldown;
            Priority = priority;
            DebuffOnly = debuff;
            MinStacks = minStacks;
        }

        public bool CanCastSkill()
        {
            return (DateTime.UtcNow - LastCast).TotalMilliseconds >= Cooldown * 1000;
        }

        internal Skill Info
        {
            get
            {
                return ObjectManager.GetSkillInfo(Id);
            }
        }

        public int Compare(ClassSkill x, ClassSkill y)
        {
            return -x.Priority.CompareTo(y.Priority);
        }

        public int CompareTo(ClassSkill other)
        {
            return -Priority.CompareTo(other.Priority);
        }
    }
}
