﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NinjaClassFightRoutine
{
    public enum Buff
    {
        Tempetedeshuriken = 52784,
        ShurikenFlamboiement = 49961,
        Sanginfecté = 52783,
    }
    public static class Buffs
    {
        public static int ToId(this Buff Buff)
        {
            return (int)Buff;
        }
    }
}
