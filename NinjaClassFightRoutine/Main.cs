﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugins;
using PluginsCommon;
using System.Diagnostics;

namespace NinjaClassFightRoutine
{
    public class Main : IPlugin
    {
        internal List<ClassSkill> Skills = new List<ClassSkill>
        {
            new ClassSkill(52701, 3.2f, 2),
            new ClassSkill(52703, 5.0f, 0),
            new ClassSkill(52702, 3.6f, 1),
        };

        public static LocalPlayer AKPlayer { get; set; }
        string AKAuthor = "Gilin";
        string AKDescription = "Awesome Sauce";
        string AKName = "AKFighter";
        Version AKVersion = typeof(Main).Assembly.GetName().Version;
        public static readonly Stopwatch WPTimerAlLClear = new Stopwatch();
        private static readonly Stopwatch MovingMessage = new Stopwatch();
        private static readonly Stopwatch StateTimer = new Stopwatch();
        private static readonly Stopwatch PluginFramerateTimer = new Stopwatch();
        internal static bool TempetedeshurikenBuff = false;
        internal static bool ShurikenFlamboiementBuff = false;
        internal static bool SanginfectéBuff = false;

        public string Name
        {
            get { return AKName; }
        }

        public string Author
        {
            get { return AKAuthor; }
        }

        public string Description
        {
            get { return AKDescription; }
        }

        public Version Version
        {
            get { return new Version(1, 0, 0, 0); }
        }

        public void OnButtonClick()
        {
        }

        public void OnStart()
        {
            Skills.Sort();
        }

        public void OnStop(bool off)
        {
        }

        public void Pulse()
        {
            Skandia.Update();
            AKPlayer = Skandia.Me;

            if (!PluginFramerateTimer.IsRunning)
            {
                PluginFramerateTimer.Start();
            }

            if (PluginFramerateTimer.ElapsedMilliseconds < 1000)
            {
                return;
            }
            else
            {
                if (!Skandia.IsInGame || AKPlayer == null)
                {
                    return;
                }
                else
                {
                    while (AKPlayer.GotTarget || AKPlayer.CurrentTarget.Info.CurrentHp <= 0)
                    {
                        foreach (var skill in Skills)
                        {
                            if (skill.DebuffOnly &&
                                    AKPlayer.CurrentTarget.Buffs.ContainsKey((ushort)skill.Info.ModifiedSkillId) &&
                                    AKPlayer.CurrentTarget.Buffs[(ushort)skill.Info.ModifiedSkillId].Stack >= skill.MinStacks)
                            {
                                continue;  
                            }
                            // here we check certain things before casting a skill, for example
                            // if we even have that skill
                            // if we are in range to cast it 
                            // and if its not in cooldown, since we have our in house cooldown check we dont need Me.CanSendSkill(skill.Id)
                            else if (AKPlayer.Skills.ContainsKey(skill.Id)
                                && !AKPlayer.InAction
                                && skill.Info.Range <= AKPlayer.CurrentTarget.Distance
                                && AKPlayer.CanSendSkill(skill.Id)
                                && skill.CanCastSkill())
                            //Me.CanSendSkill(skill.Id) &&
                            {
                                // here we actually cast it and only if it return true we move to the next cast
                                if (AKPlayer.SendSkill(skill.Id))
                                {
                                    // Only update last cast if SendSkill returns true
                                    // Keep in mind that if we are too far or 
                                    // for any other reason the skill was not casted, 
                                    // it can still return true
                                    skill.LastCast = DateTime.UtcNow;
                                    Skandia.MessageLog("Sent Skill {0}", skill.Id);
                                    break;
                                }
                            }
                        }
                        // Example of applying a debuff skill once or multiple times
                        //if (skill.DebuffOnly &&
                        //    AKPlayer.CurrentTarget.Buffs.ContainsKey((ushort)skill.Info.ModifiedSkillId) &&
                        //    AKPlayer.CurrentTarget.Buffs[(ushort)skill.Info.ModifiedSkillId].Stack >= skill.MinStacks)
                        //{
                        //    continue;
                        //}
                    }
                }
            }
        }
    }
}
                            //if (skill.DebuffOnly 
                            //    && AKPlayer.CurrentTarget.Buffs.ContainsKey((ushort)skill.Info.ModifiedSkillId) 
                            //    && AKPlayer.CurrentTarget.Buffs[(ushort)skill.Info.ModifiedSkillId].Stack >= skill.MinStacks)
                            //{
                            //    continue;
                            //}
                                    //if (skill.Id == 52707
                                    //    && AKPlayer.Skills.ContainsKey(52707)
                                    //    && TempetedeshurikenBuff == true
                                    //    && ShurikenFlamboiementBuff == true
                                    //    && SanginfectéBuff == true
                                    //    && !AKPlayer.InAction
                                    //    && skill.Info.Range <= AKPlayer.CurrentTarget.Distance
                                    //    && AKPlayer.CanSendSkill(skill.Id)
                                    //    && AKPlayer.ChakraCharges >= 3)
                                    //{
                                    //    AKPlayer.SendSkill(skill.Id);
                                    //    Skandia.MessageLog("Sending Skill {0}s, insidde laceration method", skill.Id);
                                    //    skill.LastCast = DateTime.UtcNow;
                                    //    TempetedeshurikenBuff = false;
                                    //    ShurikenFlamboiementBuff = false;
                                    //    SanginfectéBuff = false;
                                    //    break;
                                    //}
                                    //else
                                    //{
                                    //    if (AKPlayer.Skills.ContainsKey(skill.Id)
                                    //        && !AKPlayer.InAction
                                    //        && skill.Info.Range <= AKPlayer.CurrentTarget.Distance)
                                    //    {
                                    //        if (AKPlayer.CanSendSkill(skill.Id))
                                    //        {
                                    //            AKPlayer.SendSkill(skill.Id);
                                    //            Skandia.MessageLog("Sending Skill {0}s, inside normal skill method", skill.Id);
                                    //            skill.LastCast = DateTime.UtcNow;
                                    //            if (skill.Id == 52784)
                                    //            {
                                    //                TempetedeshurikenBuff = true;
                                    //            }
                                    //            else if (skill.Id == 49961)
                                    //            {
                                    //                ShurikenFlamboiementBuff = true;
                                    //            }
                                    //            else if (skill.Id == 52783)
                                    //            {
                                    //                SanginfectéBuff = true;
                                    //            }
                                    //            else
                                    //            {
                                    //                break;
                                    //            }
                                    //        }
                                    //    }
                                    //}
